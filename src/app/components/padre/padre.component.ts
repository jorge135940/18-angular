import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  Objeto1 = {
    nombre: "Goku",
    raza: "Saiyayin",
    tipo:"Anime"
  };

  Objeto2 = {
    nombre: "Superman",
    raza: "Kriptoniano",
    tipo:"Dibujo animado"
  }
  
  constructor() { }

  ngOnInit(): void {
  }
}
